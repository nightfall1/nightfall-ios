//
//  ContentView.swift
//  Nightfall-IOS
//
//  Created by Correy Winke on 1/11/20.
//  Copyright © 2020 cwinke. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
